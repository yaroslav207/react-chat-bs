import React from "react";
import Chat from "./chat/chat";
import logo from "../images/bingo.png"

function App() {
  return (
    <div className="App">
      <div className="app__header">
        <img src={logo} className="logo"></img>
      </div>
      <Chat/>
    </div>
  );
}

export default App;
