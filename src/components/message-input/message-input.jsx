import React, {useState, useEffect} from "react";
import './style.css';


function MessageInput( { prevText, sendText } ) {
    const [messageText, setMessageText] = useState(prevText.text);

    const onAddMessage = () => {
        sendText(messageText);
        setMessageText('');
    };

    useEffect(() => {
        setMessageText(prevText)
    },[prevText]);

    return (
        <div className="message-input">
            <input type="text" value={messageText} className="message-input-text" onChange={(e) => setMessageText(e.target.value)} />
            <button className="message-input-button" onClick={onAddMessage}>SEND</button>
        </div>
    );
}

export default MessageInput;