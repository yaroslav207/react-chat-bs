import React, {useEffect, useState} from "react";
import Header from "../header/header";
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";
import Preloader from "../preloader/preloader";

import {formatDateLastMessage} from "../../helpers/date.helpers";

import './style.css'

function Chat() {

    const [messages, setMessages] = useState([]);
    const [updatingMessageId, setUpdatingMessageId] = useState(null);
    const [loaded, setLoaded] = useState(false);

    const myUserId = '5328dba1-1b8f-11e8-9629-c7eca82aa7bd';

    const handleEditMessageId = (id) => {
        setUpdatingMessageId(id)
    };

    const getUserCount = (messages) => {
        const result = [];
        messages.forEach((message) => {
            if (!result.includes(message.user)) {
                result.push(message.user);
            }
        });

        return result.length
    };

    const getUser = (id) => {
        const message = messages.find(message => message.userId === id);
        if (!message) {
            return {userId: myUserId}
        }

        const {userId, user, avatar} = message;
        return {userId, user, avatar}
    };

    const getMessage = (id) => {
        return messages.find(message => message.id === id)
    };

    const getDateLastMessage = (messages) => {
        let result = messages[0].createdAt;
        messages.forEach(message => {
            if (message.createdAt > result) {
                result = message.createdAt;
            }
        });

        return result
    };

    const deleteMessage = (id) => {
        const newState = messages.filter(message => message.id !== id);
        setMessages(newState);
    };

    const editMessage = (id) => (updateMessage) => {
        const newState = messages.map(message => {
            if (message.id === id) {
                message.text = updateMessage;
                message.editedAt = new Date().toISOString();
            }
            return message
        });
        setUpdatingMessageId(null);
        setMessages(newState);
    };

    const addMessage = (messageText) => {
        const user = getUser(myUserId);
        const newMessage = {
            ...user,
            id: Date.parse(new Date()),
            text: messageText,
            createdAt: new Date().toISOString(),
            editedAt: '',
        };

        const newState = [...messages, newMessage];
        setMessages(newState);
    };

    useEffect(() => {
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(result => result.json())
            .then(json => {
                setMessages(json);
                setLoaded(true);
            })
    }, []);

    return (loaded)
        ? (
            <div className="chat">
                <Header
                    usersCount={getUserCount(messages)}
                    messageCount={messages.length}
                    lastMessageDate={formatDateLastMessage(getDateLastMessage(messages))}
                />
                <MessageList
                    messages={messages}
                    userId={myUserId}
                    deleteMessage={deleteMessage}
                    handleEditMessageId={handleEditMessageId}
                />
                <MessageInput
                    prevText={updatingMessageId ? getMessage(updatingMessageId).text : ''}
                    sendText={updatingMessageId ? editMessage(updatingMessageId) : addMessage}
                />
            </div>
        )
        : <Preloader/>
}

export default Chat;