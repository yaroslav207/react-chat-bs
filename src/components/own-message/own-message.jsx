import React from "react";
import './style.css'

function OwnMessage({ messageInfo, deleteMessage, handleEditMessageId }) {
    const onDeleteMessage = () => deleteMessage(messageInfo.id);
    const onEditMessage = () => handleEditMessageId(messageInfo.id);
    return (
        <div className="own-message">
            <div className="message-info-block">
                <span className="message-text">{ messageInfo.text }</span>
                <div className="message-time">{ messageInfo.editedAt ? `edited ${ messageInfo?.formatEditedAt }` :  messageInfo?.formatCreatedAt }</div>
                <button className="message-edit" onClick={onEditMessage}></button>
                <button className="message-delete" onClick={onDeleteMessage}></button>
            </div>
        </div>
    );
}

export default OwnMessage;